/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.gradeAnalyzer.dao;

import info.hccis.gradeAnalyzer.dao.util.ConnectionUtils;
import info.hccis.gradeAnalyzer.dao.util.DbUtils;
import info.hccis.gradeAnalyzer.model.DatabaseConnection;
import info.hccis.gradeAnalyzer.model.Grade;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jordan
 */
public class AnalyzerDAO {
    
    private final static Logger LOGGER = Logger.getLogger(AnalyzerDAO.class.getName());
    
    //write grades into the database from an ArrayList -JC
    public static void writeGrades(Connection conn, ArrayList<Grade> grades)
    {
        PreparedStatement ps = null;
        String sql = null;
        //Connection conn = null;
        try {
            //conn = ConnectionUtils.getConnection(databaseConnection);
            
            //sql to create a new row -JC
            sql = "INSERT INTO `grade`(`courseID`,`name`, `loginName`, `gradeAverage`) "
                    + "VALUES (?,?,?,?)";
            
            ps = conn.prepareStatement(sql);
            
            for(Grade grade: grades)
            {
                //set the values for the sql statement from the given grade -JC
                ps.setInt(1, grade.getCourseId());
                ps.setString(2, grade.getName());
                ps.setString(3, grade.getLoginName());
                ps.setBigDecimal(4, grade.getMark());
                
                ps.executeUpdate();
            }
            
            
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        }
        DbUtils.close(ps, conn);
    }
    
    //write a new course into the database. Only name is needed, as courseId autoincrements for new courses -JC
    public static void writeCourse(Connection conn, String courseName)
    {
        PreparedStatement ps = null;
        String sql = null;
        //Connection conn = null;
        try {
            //conn = ConnectionUtils.getConnection(databaseConnection);
            
            //sql to create a new row -JC
            sql = "INSERT INTO `course`(`CourseName`) "
                    + "VALUES (?)";
            
            ps = conn.prepareStatement(sql);
            
            //set the course name for the sql statement -JC
            ps.setString(1, courseName);
                
            ps.executeUpdate();
            
            
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        }
        DbUtils.close(ps, conn);
    }
    
    //returns the courseId of a given courseName -JC
    public static int getCourseID(DatabaseConnection databaseConnection, String courseName)
    {
        PreparedStatement ps = null;
        String sql = null; 
        int ID = 0;
        Connection conn = null;
        try {
            conn = ConnectionUtils.getConnection(databaseConnection);
            
            //sql to select rows based on courseName -JC
            sql = "SELECT * FROM course WHERE `courseName` = '" + courseName + "'";
            
            ps = conn.prepareStatement(sql);
            
            ResultSet rs = ps.executeQuery();
            
            //if a row is returned then retrieve the courseId -JC
            while(rs.next())
            {
                ID = rs.getInt("courseID");
            }
            
            
            
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        }
        DbUtils.close(ps, conn);
        
        return ID;
    }
    
    //loads an ArrayList of grades from the database based on the courseId, which can be gotten from the name above -JC
    public static ArrayList<Grade> getGrades(DatabaseConnection databaseConnection, int courseID)
    {
        PreparedStatement ps = null;
        String sql = null; 
        int ID = 0;
        Connection conn = null;
        ArrayList<Grade> grades = new ArrayList();
        try {
            conn = ConnectionUtils.getConnection(databaseConnection);
            
            //sql to get all the grades from the database based on their courseId -JC
            sql = "SELECT * FROM grade WHERE `courseID` = " + courseID + "";
            
            ps = conn.prepareStatement(sql);
            
            ResultSet rs = ps.executeQuery();
            
            //if results are returned create grade objects and load ArrayList -JC
            while(rs.next())
            {
                Grade grade = new Grade();
                grade.setCourseId(courseID);
                grade.setName(rs.getString("name"));
                grade.setLoginName(rs.getString("loginName"));
                grade.setMark(rs.getBigDecimal("gradeAverage"));
                
                grades.add(grade);
            }
            
            
            
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        }
        DbUtils.close(ps, conn);
        
        return grades;
    }
    
    //return ArrayList of courseNames from the database, used to select course for reports in view -JC
    public static ArrayList<String> getCourseNames(DatabaseConnection databaseConnection)
    {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        ArrayList<String> names = new ArrayList();
        try {
            conn = ConnectionUtils.getConnection(databaseConnection);
            
            //sql select all unique courseNames -JC
            sql = "SELECT DISTINCT courseName FROM course";
            
            ps = conn.prepareStatement(sql);
            
            
              
            ResultSet rs = ps.executeQuery();
            
            //if results are returned add the courseNames to the ArrayList -JC
            while(rs.next())
            {
                names.add(rs.getString("courseName"));
            }
            
            
            
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        }
        DbUtils.close(ps, conn);
        
        return names;
    }
}
