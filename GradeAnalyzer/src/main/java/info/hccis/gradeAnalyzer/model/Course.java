/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.gradeAnalyzer.model;

import java.math.BigDecimal;

/**
 *
 * @author Jordan
 */
public class Course {

    private String name;
    private int courseId;
    //private BigDecimal courseAverage;

    public Course() {

    }

    public Course(String name, int courseID) {
        this.name = name;
        this.courseId = courseID;
    }

    public String getName() {
        return name;
    }

    public int getCourseId() {
        return courseId;
    }

//    public BigDecimal getCourseAverage() {
//        return courseAverage;
//    }
//
//    public void setCourseAverage(BigDecimal courseAverage) {
//        this.courseAverage = courseAverage;
//    }
}
