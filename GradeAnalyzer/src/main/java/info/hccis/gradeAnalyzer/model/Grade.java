/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.gradeAnalyzer.model;

import java.math.BigDecimal;

/**
 *
 * @author Anson
 */
public class Grade {
    
    private String name, loginName;
    private int courseId;
    private BigDecimal mark;
    
    public Grade(){
        
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public BigDecimal getMark() {
        return mark;
    }

    public void setMark(BigDecimal mark) {
        this.mark = mark;
    }
    
    public int getCourseId()
    {
        return courseId;
    }
    
    public void setCourseId(int courseId)
    {
        this.courseId = courseId;
    }
    
    //write toString
}
