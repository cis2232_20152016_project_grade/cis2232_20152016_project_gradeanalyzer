package info.hccis.gradeAnalyzer.service;

import info.hccis.gradeAnalyzer.data.springdatajpa.CodeTypeRepository;
import info.hccis.gradeAnalyzer.model.DatabaseConnection;
import info.hccis.gradeAnalyzer.model.jpa.CodeType;
import java.util.ArrayList;


/**
 * Code service 
 *
 * @author BJ MacLean
 */
public interface CodeService {

    public abstract CodeTypeRepository getCtr();
    
    /**
     *
     * @param codeTypeId
     * @return
     */
    public abstract ArrayList<CodeType> getCodeTypes();
    
    /**
     *
     * @param codeTypeId
     * @return
     */
    public abstract ArrayList<CodeType> getCodeTypes(DatabaseConnection databaseConnection);
    
}
