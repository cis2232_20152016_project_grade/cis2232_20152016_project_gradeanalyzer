package info.hccis.gradeAnalyzer.util;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import org.springframework.web.multipart.MultipartFile;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jordan
 */
public class FileUtility {
    
    public static ArrayList<String> readFile(File theFile)
    {
        Scanner fileRead;//scanner to parse the file
        ArrayList<String> lines = new ArrayList();
        
        //try to read the file, catch a file not found exception
        try {
            fileRead = new Scanner(theFile);
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileUtility.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
        //get just the name of the file and add it to the first index in the array
        //String fileName  = theFile.getName();
        //fileName = fileName.substring(0, fileName.lastIndexOf('.'));
        //lines.add(fileName);
        
        //parse the file and add each row to the array
        while (fileRead.hasNext())
        {
            lines.add(fileRead.nextLine());
        }
        
        return lines;
    }
    
    //Credit to Petros Tsialiamanis
    //@http://stackoverflow.com/questions/24339990/how-to-convert-a-multipart-file-to-file
    public static File convert(MultipartFile file) throws IOException
    {    
        File convFile = new File(file.getOriginalFilename());
        convFile.createNewFile(); 
        FileOutputStream fos = new FileOutputStream(convFile); 
        fos.write(file.getBytes());
        fos.close(); 
        return convFile;
    }
    
}
