package info.hccis.gradeAnalyzer.util;

import info.hccis.gradeAnalyzer.dao.AnalyzerDAO;
import info.hccis.gradeAnalyzer.model.DatabaseConnection;
import info.hccis.gradeAnalyzer.model.Grade;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 * General program utilities
 *
 * @author bjmaclean
 * @since 20150918
 */
public class Utility {

    private static Scanner input = new Scanner(System.in);

    public static Scanner getInput() {
        return input;
    }

    //calculate the average of a course  -JC
    public static double courseAverage(DatabaseConnection databaseConnection, String courseName) {
        double sum = 0;

        //get the courseId of a course, used to get the grades for the course in question -JC
        int courseID = AnalyzerDAO.getCourseID(databaseConnection, courseName);
        //load grades based on the courseId -JC
        ArrayList<Grade> grades = AnalyzerDAO.getGrades(databaseConnection, courseID);

        //sum the grade averages -JC
        for (Grade grade : grades) {
            sum += Double.parseDouble(grade.getMark().toString());
        }

        //calculate the course average -JC
        double average = sum / grades.size();
        average = average / 100;

        return average;
    }

    //this method calculates the percentage - AC
    public static double totalPassedPercentage(DatabaseConnection databaseConnection, String courseName) {
        double totalPassed = 0;
        double count = 0;

        //get the courseId of a course, used to get the grades for the course in question - AC
        int courseID = AnalyzerDAO.getCourseID(databaseConnection, courseName);
        ArrayList<Grade> grades = AnalyzerDAO.getGrades(databaseConnection, courseID);

        //if grades are equal to or greater than 60 add to passed count - AC
        for (Grade grade : grades) {
            if (grade.getMark().doubleValue() >= 60) {
                count = count + 1;
            }
        }

        //get the average of the total passed count - AC
        totalPassed = count / grades.size();

        return totalPassed;
    }

    //this method calculates the percentage - AC
    public static double totalFailedPercentage(DatabaseConnection databaseConnection, String courseName) {
        double totalFailed = 0;
        double count = 0;

        //get the courseId of a course, used to get the grades for the course in question - AC
        int courseID = AnalyzerDAO.getCourseID(databaseConnection, courseName);
        ArrayList<Grade> grades = AnalyzerDAO.getGrades(databaseConnection, courseID);

        //if grades are less than 60 add grades to failed count - AC
        for (Grade grade : grades) {
            if (grade.getMark().doubleValue() < 60) {
                count++;
            }
        }

        //get the average of the total passed count - AC
        totalFailed = count / grades.size();

        return totalFailed;
    }

    //format the double into a percentage string - AC
    public static String formatDoubleToPercentage(double number) {
        String num;

        //http://www.avajava.com/tutorials/lessons/how-do-i-use-numberformat-to-format-a-percent.html
        //used code sample to format into percentage string - AC
        NumberFormat defaultFormat = NumberFormat.getPercentInstance();
        defaultFormat.setMinimumFractionDigits(2);
        num = defaultFormat.format(number);

        return num;
    }

    //taken from canes system to get reponce from login server
    public static String getResponseFromRest(String urlString) {
        try {

            URL url = new URL(urlString);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            String theOutput = "";
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                theOutput += output;
                System.out.println(output);
            }

            conn.disconnect();

//            ObjectMapper om = new ObjectMapper();
//            Address address = om.readValue(theOutput, Address.class);
            return theOutput;
            //JOptionPane.showMessageDialog(null, theOutput);
//            JOptionPane.showMessageDialog(null, address.toString());
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return null;
    }

}
