package info.hccis.gradeAnalyzer.web;

import info.hccis.gradeAnalyzer.dao.AnalyzerDAO;
import info.hccis.gradeAnalyzer.dao.CodeTypeDAO;
import info.hccis.gradeAnalyzer.dao.CodeValueDAO;
import info.hccis.gradeAnalyzer.dao.util.ConnectionUtils;
import info.hccis.gradeAnalyzer.data.springdatajpa.CodeTypeRepository;
import info.hccis.gradeAnalyzer.data.springdatajpa.CodeValueRepository;
import info.hccis.gradeAnalyzer.model.Course;
import info.hccis.gradeAnalyzer.model.DatabaseConnection;
import info.hccis.gradeAnalyzer.model.jpa.CodeType;
import info.hccis.gradeAnalyzer.model.jpa.CodeValue;
import info.hccis.gradeAnalyzer.model.Grade;
import info.hccis.gradeAnalyzer.model.User;
import info.hccis.gradeAnalyzer.service.CodeService;
import info.hccis.gradeAnalyzer.util.FileUtility;
import info.hccis.gradeAnalyzer.util.Utility;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import static java.math.BigDecimal.valueOf;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class CodeController {

    private final CodeService codeService;
//    private final CodeValueRepository cvr;
//    private final CodeTypeRepository ctr;

    @Autowired
    public CodeController(CodeService codeService, CodeValueRepository cvr, CodeTypeRepository ctr) {
        this.codeService = codeService;
//        this.cvr = cvr;
//        this.ctr = ctr;
    }

    @RequestMapping("/codes/codeTypes")
    public String showCodes(Model model, HttpSession session) {

        DatabaseConnection databaseConnection = (DatabaseConnection) session.getAttribute("db");
        ArrayList<CodeType> codes = codeService.getCodeTypes(databaseConnection);
        model.addAttribute("codeTypes", codes);
        return "/codes/codeTypes";
    }

    @RequestMapping("/codes/codeValues")
    public String showCodeValues(Model model, HttpServletRequest request) {
        String id = request.getParameter("id");
        System.out.println("id passed in to controller is:" + id);
        model.addAttribute("codeTypeId", id);
        //Get the code values 
        DatabaseConnection dbConnection = (DatabaseConnection) request.getSession().getAttribute("db");
        ArrayList<CodeValue> theList = CodeValueDAO.getCodeValues(dbConnection, id);
        model.addAttribute("codeValues", theList);
        System.out.println("BJM found some codeValues (" + theList.size() + ")");

        return "/codes/codeValues";
    }

    @RequestMapping("/codes/codeTypeAdd")
    public String codeTypeAdd(Model model, HttpServletRequest request) {
        DatabaseConnection dbConnection = (DatabaseConnection) request.getSession().getAttribute("db");
        System.out.println("BJM in codeTypeAdd");
        CodeType ct = new CodeType();
        model.addAttribute("codeType", ct);
        return "/codes/codeTypeEdit";
    }

    @RequestMapping("/codes/codeValueAdd")
    public String codeValueAdd(Model model, HttpServletRequest request) {
        DatabaseConnection dbConnection = (DatabaseConnection) request.getSession().getAttribute("db");
        String id = request.getParameter("id");
        System.out.println("BJM in codeValueAdd, id passed in to controller is:" + id);
        CodeValue cv = new CodeValue();
        cv.setCodeTypeId(Integer.parseInt(id));
        model.addAttribute("codeValue", cv);
        return "/codes/codeValueEdit";
    }

    @RequestMapping("/codes/codeValueEdit")
    public String codeValueEdit(Model model, HttpServletRequest request) {
        DatabaseConnection dbConnection = (DatabaseConnection) request.getSession().getAttribute("db");
        String id = request.getParameter("id");
        String sequence = request.getParameter("sequence");
        System.out.println("BJM in codeValueEdit, id passed in to controller is:" + id);
        CodeValue cv = CodeValueDAO.getCodeValue(dbConnection, id, sequence);
        model.addAttribute("codeValue", cv);
        return "/codes/codeValueEdit";
    }

    @RequestMapping("/codes/codeValueEditSubmit")
    public String codeValueEditSubmit(Model model, HttpServletRequest request, @Valid @ModelAttribute("codeValue") CodeValue codeValue, BindingResult result) {
        /*
         Testing that the int from the array is coming in from the page.
         */
        System.out.println("The int#1 is=" + codeValue.getTestInt()[0]);

        /*
         Testing that the arrayList element is also coming in from the view.  
         That the arrayList elements are bound on the form.
         */
        for (String test : codeValue.getTestIntArrayList()) {
            System.out.println("The value of array list string=" + test);
        }

        System.out.println("Made it to code value edit submit, description=" + codeValue.getEnglishDescription());
        if (result.hasErrors()) {
            System.out.println("Error in validation of code value.");
            return "/codes/codeValueEdit";
        }
        DatabaseConnection dbConnection = (DatabaseConnection) request.getSession().getAttribute("db");
        CodeValueDAO.addUpdate(dbConnection, codeValue);

        //next send them back to the code values for the code type.
        ArrayList<CodeValue> theList = CodeValueDAO.getCodeValues(dbConnection, String.valueOf(codeValue.getCodeTypeId()));
//        //use jpa
//        ArrayList<CodeValue> theList = (ArrayList<CodeValue>) cvr.findByCodeTypeId(codeValue.getCodeTypeId());
        model.addAttribute("codeValues", theList);

        model.addAttribute("codeTypeId", codeValue.getCodeTypeId());
        //send to the codeValues view.
        return "/codes/codeValues";
    }

    @RequestMapping("/codes/codeTypeEditSubmit")
    public String codeTypeEditSubmit(Model model, HttpServletRequest request, @Valid @ModelAttribute("codeType") CodeType codeType, BindingResult result) {

        System.out.println("Made it to code type edit submit, description=" + codeType.getEnglishDescription());
        if (result.hasErrors()) {
            System.out.println("Error in validation of code type.");
            return "/codes/codeTypeEdit";
        }
        DatabaseConnection dbConnection = (DatabaseConnection) request.getSession().getAttribute("db");
        CodeTypeDAO.add(dbConnection, codeType, "ADMIN");
        //use jpa...no since using database connection provided on page.
//        ctr.save(codeType);

        //use jpa
        //ArrayList<CodeType> theList = (ArrayList<CodeType>) ctr.findAll();
        ArrayList<CodeType> theList = CodeTypeDAO.getCodeTypes(dbConnection);
        model.addAttribute("codeTypes", theList);

        //send to the codeTypes view.
        return "/codes/codeTypes";
    }

    //
    //All GradeAnalyzer Relevent code below here 
    //
    //mapping to upload a tsv file to load the data for a course -JC
    @RequestMapping("/utility/importFile")
    public String importFile(Model model, HttpSession session, HttpServletRequest request) {
        HttpSession ssh = request.getSession();

        if (ssh.getAttribute("login") != null) {
            //remove any error message that may be set for courseSelect - AC
            String error = "";
            session.setAttribute("error", error);
            return "/utility/importFile";
        } else {
            return "redirect:/";
        }
    }

    //mapping for submitting the uploaded file -JC
    @RequestMapping("/utility/importSubmit")
    public String importSubmit(Model model, HttpSession session, HttpServletRequest request, @RequestParam("myFile") MultipartFile file) {
        HttpSession ssh = request.getSession();

        if (ssh.getAttribute("login") != null) {
            DatabaseConnection dbConnection = (DatabaseConnection) request.getSession().getAttribute("db");
            String successText = "";

            //get the original name of the file as a String -JC
            String name = file.getOriginalFilename();
            //remove the extension from the file name for storage in the database -JC
            name = name.substring(0, name.lastIndexOf('.'));
            //declare a file object -JC
            File uploadedFile = null;

            //convert the MultiPartFile from the view into a usable File object -JC
            try {
                uploadedFile = FileUtility.convert(file);
            } catch (IOException exc) {
                System.out.println(exc.getMessage());
            }
            //declare an int to hold the courseId for the grades -JC
            int ID = 0;

            //read the lines from the file -JC
            ArrayList<String> lines = FileUtility.readFile(uploadedFile);
            if (lines.size() == 0) {
                //set text to be displayed to view after file upload fails -JC
                successText = "File upload failed.";
                //set the text to session -JC
                session.setAttribute("successText", successText);

                return "/utility/importFileSuccess";
            }

            try {
                //write the course to the database, with the name obtained from the file -JC
                AnalyzerDAO.writeCourse(ConnectionUtils.getConnection(), name);
            } catch (Exception ex) {
                Logger.getLogger(CodeController.class.getName()).log(Level.SEVERE, null, ex);
            }

            //set the courseId to the autogenerated ID from the course object -JC
            ID = AnalyzerDAO.getCourseID(dbConnection, name);

            //remove the top line which holds the exported headers -JC
            lines.remove(0);
            //declare an ArrayList to hold grade objects -JC
            ArrayList<Grade> grades = new ArrayList();
            //traverse the lines returned from the file and set up grade objects -JC
            for (String line : lines) {
                //split the lines on the tab character -JC
                String[] student = line.split("\t");
                int count = 0;
                double sum = 0;
                double average = 0;
                //set the name and loginName for each grade -JC
                String studentName = student[0];

                //remove quotes from name
                studentName = studentName.substring(1, studentName.length() - 1);
                String loginName = student[1];

                //calculate the average -JC
                for (int i = 4; i < 15; i++) {
                    sum += Double.parseDouble(student[i]);
                    count++;
                }

                average = sum / count;

                //create the grade object -JC
                Grade grade = new Grade();
                grade.setCourseId(ID);
                grade.setName(studentName);
                grade.setLoginName(loginName);
                grade.setMark(BigDecimal.valueOf(average));
                //add the grade object to the ArrayList -JC
                grades.add(grade);
            }

            try {
                //write the grades to the database -JC
                AnalyzerDAO.writeGrades(ConnectionUtils.getConnection(), grades);
                //if code gets this far then file upload was successful so set the text to tell the user so -JC
                successText = "File upload successful!";
                session.setAttribute("successText", successText);
            } catch (Exception ex) {
                Logger.getLogger(CodeController.class.getName()).log(Level.SEVERE, null, ex);
            }

            //go to post-upload page -JC
            return "/utility/importFileSuccess";
        } else {
            return "redirect:/";
        }
    }

    //mapping to show success or fail message after uploading a file -JC
    @RequestMapping("/utility/importFileSuccess")
    public String importFileSuccess(Model model, HttpSession session, HttpServletRequest request) {
        HttpSession ssh = request.getSession();

        if (ssh.getAttribute("login") != null) {
            //there is a button on this page to return to file upload page and a message, but that's about it. -JC
            return "/utility/importFileSuccess";
        } else {
            return "redirect:/";
        }
    }

    @RequestMapping(value = "/loginProcess", method = RequestMethod.POST)
    public String doLogin(Model model, HttpSession session, @ModelAttribute("user") User user) {
        String accessUrl = "http://localhost:8080/cisadminRemote/rest/useraccess/codes," + user.getUsername() + "," + user.getPassword();
        String responce = Utility.getResponseFromRest(accessUrl);

        if (responce != null && !responce.equals("0")) {
            session.setAttribute("login", responce);
            return "redirect:/utility/importFile";
        } else {
            model.addAttribute("login", "0");
            model.addAttribute("message", "Login failed");
            return "other/login";
        }
    }

    @RequestMapping("/logout")
    public String doLogout(Model model, HttpSession session, HttpServletRequest request) {
        HttpSession login = request.getSession();

        if (login.getAttribute("login") != null) {
            login.removeAttribute("login");
            return "redirect:/";
        } else {
            return "redirect:/";
        }

    }

    @RequestMapping("/course/courseSelect")
    public String courseSelect(Model model, HttpSession session, HttpServletRequest request) {
        HttpSession ssh = request.getSession();

        if (ssh.getAttribute("login") != null) {

            DatabaseConnection dbConnection = (DatabaseConnection) request.getSession().getAttribute("db");

            //setup list of courses for the select course screen - AC
            ArrayList<String> courseList = new ArrayList<String>();

            //add courses into arrayList - AC
            courseList = AnalyzerDAO.getCourseNames(dbConnection);

            //add list of courses to the model - AC
            model.addAttribute("courseList", courseList);

            return "/course/courseSelect";

        } else {
            return "redirect:/";
        }
    }

    //this creates a page with a dropdown box of course Id's
    @RequestMapping("/course/courseSelected")
    public String courseSelected(Model model, HttpServletRequest request, HttpSession session, @Valid @ModelAttribute("course") String name, BindingResult result) {
        HttpSession ssh = request.getSession();

        if (ssh.getAttribute("login") != null) {

            DatabaseConnection dbConnection = (DatabaseConnection) request.getSession().getAttribute("db");

            //declaring variables - AC
            double courseAverage;
            double totalPassed;
            double totalFailed;
            String courseName = name;
            int courseId = 0;

            //set up number formatter in order to format numbers - AC
            NumberFormat defaultFormat = NumberFormat.getPercentInstance();
            defaultFormat.setMinimumFractionDigits(2);

            //check to see if a course is selected - AC
            if (!"None".equals(courseName)) {
                //set up session error string - AC
                String error = "";
                session.setAttribute("error", error);

                //getting the total passed for the course selected - AC
                courseId = AnalyzerDAO.getCourseID(dbConnection, courseName);

                //add total passed percentage to the model - AC
                model.addAttribute("courseId", courseId);

                //add course name to session - AC
                session.setAttribute("courseName", courseName);

                //getting the average for the course selected - AC
                courseAverage = Utility.courseAverage(dbConnection, courseName);

                //format for percentage - AC
                String courseAverageString = Utility.formatDoubleToPercentage(courseAverage);
                //add the average to the model    
                model.addAttribute("courseAverage", courseAverageString);

                //getting the total passed for the course selected - AC
                totalPassed = Utility.totalPassedPercentage(dbConnection, courseName);
                //format for percentage - AC
                String totalPassedString = Utility.formatDoubleToPercentage(totalPassed);
                //add total passed percentage to the model - AC
                model.addAttribute("totalPassed", totalPassedString);

                //getting the total passed for the course selected - AC
                totalFailed = Utility.totalFailedPercentage(dbConnection, courseName);
                //format for percentage - AC
                String totalFailedString = Utility.formatDoubleToPercentage(totalFailed);
                //add total passed percentage to the model - AC
                model.addAttribute("totalFailed", totalFailedString);

                return "/course/courseReport";

            } else {
                //set session error string - AC
                String error = "You Must Select a Course";
                session.setAttribute("error", error);

                //setup list of courses for the select course screen - AC
                ArrayList<String> courseList = new ArrayList<String>();

                //setup list of courses - AC
                courseList = AnalyzerDAO.getCourseNames(dbConnection);

                //add list of courses to the courstList - AC
                model.addAttribute("courseList", courseList);

                return "/course/courseSelect";
            }
        } else {
            return "redirect:/";
        }

    }

    //this outputs course information into a course report page
    @RequestMapping("/course/courseReport")
    public String courseReport(Model model, HttpSession session, HttpServletRequest request) {
        HttpSession ssh = request.getSession();

        if (ssh.getAttribute("login") != null) {
            //remove any error message that may have been saved in session - AC
            String error = "";
            session.setAttribute("error", error);

            //loads page that displays data from the selected course - AC
            return "/course/courseReport";
        } else {
            return "redirect:/";
        }
    }

    @RequestMapping("/grades/gradeList")
    public String gradeList(Model model, HttpServletRequest request, HttpSession session) {
        HttpSession ssh = request.getSession();

        if (ssh.getAttribute("login") != null) {
            DatabaseConnection dbConnection = (DatabaseConnection) request.getSession().getAttribute("db");

            //set courseName equal to model attribute - AC
            String courseName = (String) session.getAttribute("courseName");

            //get the courseId based on courseName - AC
            int courseId = AnalyzerDAO.getCourseID(dbConnection, courseName);

            //add grades for the course to an arrayList - AC
            ArrayList<Grade> gradesList = new ArrayList();
            gradesList = AnalyzerDAO.getGrades(dbConnection, courseId);

            //send arraylist to view - AC
            session.setAttribute("gradesList", gradesList);

            return "/grades/gradeList";
        } else {
            return "redirect:/";
        }
    }

}
