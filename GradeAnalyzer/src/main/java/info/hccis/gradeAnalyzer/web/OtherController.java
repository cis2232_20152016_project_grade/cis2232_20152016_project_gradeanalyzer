package info.hccis.gradeAnalyzer.web;

import info.hccis.gradeAnalyzer.data.springdatajpa.CodeTypeRepository;
import info.hccis.gradeAnalyzer.data.springdatajpa.CodeValueRepository;
import info.hccis.gradeAnalyzer.model.DatabaseConnection;
import info.hccis.gradeAnalyzer.model.User;
import info.hccis.gradeAnalyzer.model.jpa.CodeType;
import info.hccis.gradeAnalyzer.model.jpa.CodeValue;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class OtherController {

    private final CodeTypeRepository ctr;
    private final CodeValueRepository cvr;

    @Autowired
    public OtherController(CodeTypeRepository ctr, CodeValueRepository cvr){ 
        this.ctr = ctr;
        this.cvr = cvr;
    }

    @RequestMapping("/other/futureUse")
    public String showFutureUse(Model model) {
        return "other/futureUse";
    }

    @RequestMapping("/other/about")
    public String showAbout(Model model) {
       
        return "other/about";
    }

    @RequestMapping("/other/help")
    public String showHelp(Model model) {
        return "other/help";
    }

    @RequestMapping("/")
    public String showHome(Model model) {
        User user = new User();
        model.addAttribute("user", user);
        return "other/login";
    }

}
