/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.gradeAnalyzer.web.services;

import static com.sun.jersey.core.header.FormDataContentDisposition.name;
import info.hccis.gradeAnalyzer.dao.CodeValueDAO;
import info.hccis.gradeAnalyzer.model.DatabaseConnection;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author bjmaclean
 */
@WebService(serviceName = "CodeValue2")
public class CodeValue2 {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "getCodeValueDescription")
    public String hello(@WebParam(name = "codeTypeId") int codeTypeId, @WebParam(name = "codeValueSequence") int codeValueSequence) {
        DatabaseConnection dbc = new DatabaseConnection();
        return CodeValueDAO.getCodeValueDescription(dbc, codeTypeId, codeValueSequence);
        
    }
}
