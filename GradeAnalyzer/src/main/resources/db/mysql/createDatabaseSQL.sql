--
-- Database: `GradeAnalyzer`
--

CREATE database  IF NOT EXISTS `GradeAnalyzer`;
use `GradeAnalyzer`;


CREATE TABLE IF NOT EXISTS `Grade` (
  `courseId` int(6) NOT NULL COMMENT 'This is the course id',
  `name` varchar(100) NOT NULL COMMENT 'Last and First Name',
  `loginName` varchar(50) NOT NULL COMMENT 'Login Name',
  `gradeAverage` decimal(4,2) NOT NULL COMMENT 'this is the grade average'
) DEFAULT CHARSET=latin1 COMMENT='this holds course information';

--
-- Dumping data for table `Grade`
--


--
-- creating table `Course`
--

CREATE TABLE IF NOT EXISTS `Course` (
  `courseId` int(6) NOT NULL AUTO_INCREMENT COMMENT 'This is the course id',
  `courseName` varchar(100) NOT NULL COMMENT 'Course Name',
   primary key (courseId)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COMMENT='this holds course information';



--
-- Table structure for table `useraccess`
--

CREATE TABLE IF NOT EXISTS `useraccess` (
  `userAccessId` int(3) NOT NULL,
  `username` varchar(100) NOT NULL COMMENT 'Unique user name for app',
  `password` varchar(128) NOT NULL,
  `userTypeCode` int(3) NOT NULL COMMENT 'Code type #1',
  `createdDateTime` datetime DEFAULT NULL COMMENT 'When user was created.'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `useraccess`
--
ALTER TABLE `useraccess`
  ADD PRIMARY KEY (`userAccessId`);

--
-- AUTO_INCREMENT for table `useraccess`
--
ALTER TABLE `useraccess`
  MODIFY `userAccessId` int(3) NOT NULL AUTO_INCREMENT;