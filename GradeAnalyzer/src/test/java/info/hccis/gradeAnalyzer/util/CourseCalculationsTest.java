/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.gradeAnalyzer.util;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import info.hccis.gradeAnalyzer.model.Grade;
import java.math.BigDecimal;

/**
 *
 * @author Anson
 */
public class CourseCalculationsTest {

    public CourseCalculationsTest() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    //Testing the totalPassedPercentage - AC
    @Test
    public void totalPassedPercentageIs25() {
        //declaring test data - AC
        double totalPassed = 0;
        double count = 0;
        BigDecimal mark1 = BigDecimal.valueOf(22.22);
        BigDecimal mark2 = BigDecimal.valueOf(33.33);
        BigDecimal mark3 = BigDecimal.valueOf(44.44);
        BigDecimal mark4 = BigDecimal.valueOf(55.55);
        BigDecimal mark5 = BigDecimal.valueOf(66.66);
        BigDecimal mark6 = BigDecimal.valueOf(77.77);
        BigDecimal mark7 = BigDecimal.valueOf(44.44);
        BigDecimal mark8 = BigDecimal.valueOf(44.44);

        //creating test grade objects - AC
        Grade grade1 = new Grade();
        grade1.setMark(mark1);
        Grade grade2 = new Grade();
        grade2.setMark(mark2);
        Grade grade3 = new Grade();
        grade3.setMark(mark3);
        Grade grade4 = new Grade();
        grade4.setMark(mark4);
        Grade grade5 = new Grade();
        grade5.setMark(mark5);
        Grade grade6 = new Grade();
        grade6.setMark(mark6);
        Grade grade7 = new Grade();
        grade7.setMark(mark7);
        Grade grade8 = new Grade();
        grade8.setMark(mark8);

        //creating arraylist of grades which holds test data - AC
        ArrayList<Grade> grades = new ArrayList<Grade>();
        grades.add(grade1);
        grades.add(grade2);
        grades.add(grade3);
        grades.add(grade4);
        grades.add(grade5);
        grades.add(grade6);
        grades.add(grade7);
        grades.add(grade8);

        //if grades are equal to or greater than 60 add to passed count - AC
        for (Grade grade : grades) {

            if (grade.getMark().doubleValue() >= 60) {
                count = count + 1;
            }
        }

        try {
            //passing arraylists into gradeAverage method - AC
            //get the average of the total passed count - AC
            totalPassed = count / grades.size();

        } catch (Exception ex) {
            Logger.getLogger(UtilityTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Compare gradeAverage to expected results - AC
        assertEquals(0.25, 0.0001, totalPassed);
    }

    //Testing the totalFailedPercentage
    @Test
    public void totalFailedPercentageIs75() {
        double totalPassed = 0;
        double count = 0;
        //declaring test data - AC
        BigDecimal mark1 = BigDecimal.valueOf(22.22);
        BigDecimal mark2 = BigDecimal.valueOf(33.33);
        BigDecimal mark3 = BigDecimal.valueOf(44.44);
        BigDecimal mark4 = BigDecimal.valueOf(55.55);
        BigDecimal mark5 = BigDecimal.valueOf(66.66);
        BigDecimal mark6 = BigDecimal.valueOf(77.77);
        BigDecimal mark7 = BigDecimal.valueOf(44.44);
        BigDecimal mark8 = BigDecimal.valueOf(44.44);

        //creating grade objects - AC
        Grade grade1 = new Grade();
        grade1.setMark(mark1);
        Grade grade2 = new Grade();
        grade2.setMark(mark2);
        Grade grade3 = new Grade();
        grade3.setMark(mark3);
        Grade grade4 = new Grade();
        grade4.setMark(mark4);
        Grade grade5 = new Grade();
        grade5.setMark(mark5);
        Grade grade6 = new Grade();
        grade6.setMark(mark6);
        Grade grade7 = new Grade();
        grade7.setMark(mark7);
        Grade grade8 = new Grade();
        grade8.setMark(mark8);

        //creating arraylist of grade objects for testing - AC
        ArrayList<Grade> grades = new ArrayList<Grade>();
        grades.add(grade1);
        grades.add(grade2);
        grades.add(grade3);
        grades.add(grade4);
        grades.add(grade5);
        grades.add(grade6);
        grades.add(grade7);
        grades.add(grade8);

        //if grades are equal to or greater than 60 add to passed count - AC
        for (Grade grade : grades) {

            if (grade.getMark().doubleValue() < 60) {
                count = count + 1;
            }
        }

        try {
            //passing arraylists into gradeAverage method - AC
            //get the average of the total passed count - AC
            totalPassed = count / grades.size();

        } catch (Exception ex) {
            Logger.getLogger(UtilityTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Compare gradeAverage to expected results - AC
        assertEquals(0.75, 0.0001, totalPassed);
    }
}
